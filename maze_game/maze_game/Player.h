/*

	Player.h
	maze_game

	Created by Jaxon Kneipp.
	Copyright � 2017 Jaxon Kneipp. All rights reserved.

*/

// Include libraries and required objects
#pragma once
#include <string>

using namespace std;

// Declaration of Maze class
class Player {

	public:

		string name;

		int position[2] = { 0, 0 };

};
