/*

	Maze.cpp
	maze_game

	Created by Jaxon Kneipp.
	Copyright � 2017 Jaxon Kneipp. All rights reserved.

*/

// Include libraries and required objects
#include "Maze.h"
#include <iostream>
#include <Windows.h>
#include <string>
#include <cstdio>
#include <ctime>
#include <fstream>
#include <sstream>

using namespace std;

/*-----------------------------------------------------------------------------------------*/
/*	load_current_level loads a maze from an text file
/*-----------------------------------------------------------------------------------------*/
void Maze::load_current_level() {

	ifstream fin;
	char current_character;
	int row = 0;
	int column = 0;

	// Create file name based on level
	stringstream ss;
	ss << "level_" << Maze::level << "_maze.txt";

	fin.open(ss.str());

	while (!fin.eof()) {

		fin.get(current_character);

		// Check current character
		switch (current_character) {

		case ' ':

			// Space

			Maze::bitmap[row][column] = 0;
			column += 1;

			break;

		case '\n':

			// New row

			column = 0;
			row += 1;

			break;

		default:

			// Wall

			Maze::bitmap[row][column] = 1;
			column += 1;

			break;

		}

	}

	fin.close();

	// Initialise player position
	Maze::bitmap[Maze::player.position[0]][Maze::player.position[1]] = 2;

	// Initialise finishing location
	Maze::bitmap[Maze::level_ending_position[Maze::level-1][0]][Maze::level_ending_position[Maze::level - 1][1]] = 3;

}


/*-----------------------------------------------------------------------------------------*/
/*	move_character handles the movement of the player around the maze
/*-----------------------------------------------------------------------------------------*/
void Maze::move_character(char direction_code) {

	DWORD dw;
	COORD here;
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

	// Ensure the console handle is valid
	if (hStdOut == INVALID_HANDLE_VALUE) {

		printf("Invalid handle");

	}

	switch (direction_code) {

	case 'u':

		// MOVE PLAYER UP

		// Check if move is able to be made
		if (Maze::bitmap[Maze::player.position[0] - 1][Maze::player.position[1]] == 0 || Maze::bitmap[Maze::player.position[0] - 1][Maze::player.position[1]] == 3) {

			// HANDLE BITMAP REARANGEMENT AND DISPLAY REDRAWING

			Maze::bitmap[Maze::player.position[0]][Maze::player.position[1]] = 0;

			here.X = Maze::player.position[1];
			here.Y = Maze::player.position[0];
			WriteConsoleOutputCharacter(hStdOut, L" ", 1, here, &dw);

			Maze::player.position[0] -= 1;

			Maze::bitmap[Maze::player.position[0]][Maze::player.position[1]] = 2;

			here.X = Maze::player.position[1];
			here.Y = Maze::player.position[0];
			WriteConsoleOutputCharacter(hStdOut, L"@", 1, here, &dw);

		}
		else {

			// PLAYER HIT WALL

			system("Color C0");
			Beep(523, 500);
			system("Color 0F");

			Maze::score -= 1;

		}

		break;

	case 'd':

		// MOVE PLAYER DOWN

		// Check if move is able to be made
		if (Maze::bitmap[Maze::player.position[0] + 1][Maze::player.position[1]] == 0 || Maze::bitmap[Maze::player.position[0] + 1][Maze::player.position[1]] == 3) {

			// HANDLE BITMAP REARANGEMENT AND DISPLAY REDRAWING

			Maze::bitmap[Maze::player.position[0]][Maze::player.position[1]] = 0;

			here.X = Maze::player.position[1];
			here.Y = Maze::player.position[0];
			WriteConsoleOutputCharacter(hStdOut, L" ", 1, here, &dw);

			Maze::player.position[0] += 1;

			Maze::bitmap[Maze::player.position[0]][Maze::player.position[1]] = 2;

			here.X = Maze::player.position[1];
			here.Y = Maze::player.position[0];
			WriteConsoleOutputCharacter(hStdOut, L"@", 1, here, &dw);

		}
		else {

			// PLAYER HIT WALL

			system("Color C0");
			Beep(523, 500);
			system("Color 0F");

			Maze::score -= 1;

		}

		break;

	case 'l':

		// MOVE PLAYER LEFT

		// Check if move is able to be made
		if (Maze::player.position[1] - 1 >=0 && Maze::bitmap[Maze::player.position[0]][Maze::player.position[1] - 1] == 0 || Maze::bitmap[Maze::player.position[0]][Maze::player.position[1]-1] == 3) {

			// HANDLE BITMAP REARANGEMENT AND DISPLAY REDRAWING

			Maze::bitmap[Maze::player.position[0]][Maze::player.position[1]] = 0;

			here.X = Maze::player.position[1];
			here.Y = Maze::player.position[0];
			WriteConsoleOutputCharacter(hStdOut, L" ", 1, here, &dw);

			Maze::player.position[1] = Maze::player.position[1] - 1;

			Maze::bitmap[Maze::player.position[0]][Maze::player.position[1]] = 2;

			here.X = Maze::player.position[1];
			here.Y = Maze::player.position[0];
			WriteConsoleOutputCharacter(hStdOut, L"@", 1, here, &dw);

		}
		else {

			// PLAYER HIT WALL

			system("Color C0");
			Beep(523, 500);
			system("Color 0F");

			Maze::score -= 1;

		}

		break;

	case 'r':

		// MOVE PLAYER RIGHT

		// Check if move is able to be made
		if (Maze::bitmap[Maze::player.position[0]][Maze::player.position[1] + 1] == 0 || Maze::bitmap[Maze::player.position[0]][Maze::player.position[1]+1] == 3) {

			// HANDLE BITMAP REARANGEMENT AND DISPLAY REDRAWING

			Maze::bitmap[Maze::player.position[0]][Maze::player.position[1]] = 0;

			here.X = Maze::player.position[1];
			here.Y = Maze::player.position[0];
			WriteConsoleOutputCharacter(hStdOut, L" ", 1, here, &dw);

			Maze::player.position[1] += 1;

			Maze::bitmap[Maze::player.position[0]][Maze::player.position[1]] = 2;

			here.X = Maze::player.position[1];
			here.Y = Maze::player.position[0];
			WriteConsoleOutputCharacter(hStdOut, L"@", 1, here, &dw);

		}
		else {

			// PLAYER HIT WALL
			
			system("Color C0");
			Beep(523, 500);
			system("Color 0F");

			Maze::score -= 1;

		}

		break;

	default:

		break;

	}

	// Check if player is in finishing position
	if (Maze::player.position[0] == Maze::level_ending_position[Maze::level - 1][0] && Maze::player.position[1] == Maze::level_ending_position[Maze::level - 1][1]) {

		Maze::completed = true;

	}

}

void Maze::print() {

	// Loop through bitmap rows
	for (int row = 0; row < 40; row++) {

		// Ensure row is part of the navigatable maze
		if (!Maze::is_empty_row(bitmap[row])) {

			// Loop through bitmap columns
			for (int column = 0; column < Maze::side_length; column++) {

				if (Maze::bitmap[row][column] == 1) {

					// DRAW WALL

					cout << (char)219;

				}
				else if (Maze::bitmap[row][column] == 2) {

					// DRAW PLAYER

					cout << "@";

				}
				else if (Maze::bitmap[row][column] == 3) {

					// DRAW FINISH

					cout << (char)184;

				}
				else {

					// DRAW SPACE

					cout << " ";

				}

			}

			// Start new row
			cout << "\n";

		}

	}

}

bool Maze::is_empty_row(int row[70]) {

	// Loop through all columns
	for(int index = 0; index < 70; index++) {

		// Check if a non-space exists
		if (row[index] != 0) {

			return false;

		}

	}

	return true;

}