/*

	Main.cpp
	maze_game

	Created by Jaxon Kneipp.
	Copyright � 2017 Jaxon Kneipp. All rights reserved.

*/

// Include libraries and required objects
#include <iostream>
#include <Windows.h>
#include <string>
#include <fstream>
#include <time.h>
#include <conio.h>
#include <mmsystem.h>
#include "Maze.h"
#include "Player.h"

#pragma comment(lib, "winmm.lib")

using namespace std;

// Global variables
Maze maze;
HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

/*-----------------------------------------------------------------------------------*/
/*	save_player_data mananges the leaderboard and saving the player's score
/*-----------------------------------------------------------------------------------*/
void save_player_data() {	

	/*-----------------------*/
	/*	Get scores from file
	/*-----------------------*/

	ifstream fin;
	string current_line;
	string leaderboard_names[5];
	int leaderboard_scores[5];
	int current_index = 0;
	bool is_name = true;

	// Open leaderboard.txt
	fin.open("leaderboard.txt");

	// Loop through text file line by line
	while (getline(fin, current_line)) {

		if (is_name) {

			// Current line contains a player's name
			
			leaderboard_names[current_index] = current_line;

		}
		else {

			// Current line contains a player's score

			leaderboard_scores[current_index] = stoi(current_line);

			current_index += 1;

		}

		is_name = !is_name;

	}

	/*-----------------------*/
	/*	Add players data
	/*-----------------------*/

	// Determine if players score should be on the leaderboard
	if (maze.score > leaderboard_scores[4]) {

		int insertion_index = 0;

		// Loop through to find players rank
		for (insertion_index; insertion_index <= 4; insertion_index++) {

			if (maze.score > leaderboard_scores[insertion_index]) {

				break;

			}

		}

		string temp_name = maze.player.name;
		int temp_score = maze.score;

		// Loop through to insert and shift array items to update the leaderboard
		for (insertion_index; insertion_index <= 4; insertion_index++) {

			string temp_name_dup = leaderboard_names[insertion_index];
			int temp_score_dup = leaderboard_scores[insertion_index];

			leaderboard_names[insertion_index] = temp_name;
			leaderboard_scores[insertion_index] = temp_score;

			temp_name = temp_name_dup;
			temp_score = temp_score_dup;

		}

	}

	/*-----------------------*/
	/*	Save data to file
	/*-----------------------*/

	ofstream fout;
	fout.open("leaderboard.txt");

	// Loop through current leaderboard names and scores
	for (int index = 0; index <= 4; index++) {

		if (leaderboard_scores[index] >= 0) {

			// Write players name and score to file
			fout << leaderboard_names[index] << endl;
			fout << leaderboard_scores[index] << endl;

		}

	}

}

/*-----------------------------------------------------------------------------------*/
/*	end_level handles the completion of a level
/*-----------------------------------------------------------------------------------*/

void end_level() {

	// Clear the display
	system("cls");

	/*---------------------------------*/
	/*	Output level complete title
	/*---------------------------------*/

	ifstream fin;
	string current_line;

	fin.open("level_complete_title.txt");

	while (getline(fin, current_line)) {

		cout << current_line << endl;

	};

	cout << endl << endl;

	// Display congradulatory message with player's name and current score
	cout << "CONGRADULATIONS! " << maze.player.name << ", you have completed a level! Your current score is " << maze.score << ", remember the lower your score the better!!" << endl << endl;

	if (maze.level == maze.amount_of_levels) {

		// Player has complete the final maze level

		cout << "You have now completed all available levels. (Press any key to continue).";
		_getch();

	} else {
	
		// Player has more levels to complete

		cout << "Ready for the next level? (Press any key to continue) ";

		_getch();

		cout << endl;
		cout << "Ready..." << endl;
		Sleep(1000);
		cout << "Set..." << endl;
		Sleep(1000);
		cout << "GO!" << endl;
		Sleep(1000);
	
	}

	// Clear display
	system("cls");

}

/*-----------------------------------------------------------------------------------*/
/*	load_level sets up the current level ready for play
/*-----------------------------------------------------------------------------------*/

void load_level() {

	maze.load_current_level();
	maze.print();

}

/*-----------------------------------------------------------------------------------*/
/*	enable_movement allows the player to naviagte the maze and complete the level
/*-----------------------------------------------------------------------------------*/

void enable_movement() {

	// Start timer from level
	clock_t start;
	start = clock();
	double duration;

	// Loop until maze has been completed
	while (!maze.completed) {

		// Detect key presses
		int c = 0;
		c = 0;
		c = _getch();

		if (c == 72) {

			// UP KEY PRESSED 

			maze.move_character('u');

		}
		else if (c == 80) {

			// DOWN KEY PRESSED

			maze.move_character('d');

		}
		else if (c == 75) {

			// LEFT KEY PRESSED

			maze.move_character('l');

		}
		else if (c == 77) {

			// RIGHT KEY PRESSED

			maze.move_character('r');

		}

	}

	// Stop timer and calculate completion time (in seconds)
	duration = (clock() - start) / (double)CLOCKS_PER_SEC;

	// Add calculated score for level to the player's total score
	maze.score += floor((100.0/duration)*10);

	// Ensure score is not negative
	if (maze.score < 0) {

		maze.score = 0;

	}

}

/*-----------------------------------------------------------------------------------------*/
/*	initialise_game introduces the game, resets main values and gets input from the player
/*-----------------------------------------------------------------------------------------*/

void initialise_game() {

	// Play main arcade music
	PlaySound(TEXT("main.wav"), NULL, SND_ASYNC | SND_FILENAME | SND_LOOP);

	// Set text colour to green
	SetConsoleTextAttribute(hConsole, 10);

	/*-----------------------*/
	/*	Reset values
	/*-----------------------*/

	maze.player.name = "";
	maze.score = 0;
	
	/*-----------------------*/
	/*	Output main title
	/*-----------------------*/

	ifstream fin;
	char current_character;

	fin.open("title.txt");

	while (!fin.eof()) {

		fin.get(current_character);

		cout << current_character;

	};

	// Set text colour to white
	SetConsoleTextAttribute(hConsole, 15);

	// Display  welcome message
	cout << "WELCOME to maze game! The instructions are simple, use the arrow keys to naviagte your way to the copyright sign as quick as possible. The faster you get to the center the more points you earn! Avoid bumping into walls because that will take some of your precious points away :(\n\nGood luck!!\n\n";


	/*-----------------------*/
	/*	Output leaderboard
	/*-----------------------*/

	cout << "LEADER BOARD" << endl;

	ifstream fin_leaderboard;
	string current_line;

	int current_index = 0;

	fin_leaderboard.open("leaderboard.txt");

	bool is_name = true;
	int rank = 1;

	while (getline(fin_leaderboard, current_line)) {

		if (is_name) {

			cout << rank << ". " << current_line << " with ";

		}
		else {

			cout << current_line << endl;

			rank += 1;

		}

		is_name = !is_name;

	}

	cout << endl;

	/*-----------------------*/
	/*	Get players name     
	/*-----------------------*/

	cout << "To continue please enter a nickname: ";

	getline(cin, maze.player.name);

	cout << endl;
	cout << "Welcome " << maze.player.name << ". There are " << maze.amount_of_levels << " levels in this game, complete them all as fast as possible get the best score (in this case, the smaller you points the better). Lets, start with the first one." << endl << endl;

	cout << "Ready? (Press any key to continue) ";

	_getch();

	cout << endl;
	cout << "Ready..." << endl;
	Sleep(1000);
	cout << "Set..." << endl;
	Sleep(1000);
	cout << "GO!" << endl;
	Sleep(1000);

	// Clear the display
	system("cls");

}

/*-----------------------------------------------------------------------------------*/
/*	playGame starts the game
/*-----------------------------------------------------------------------------------*/

void playGame() {

	initialise_game();

	for (int level = 1; level <= maze.amount_of_levels; level++) {

		// Set relevent variables
		maze.level = level;
		maze.completed = false;
		maze.player.position[0] = maze.level_starting_positions[level-1][0];
		maze.player.position[1] = maze.level_starting_positions[level-1][1];

		load_level();

		enable_movement();

		end_level();

	}

	save_player_data();

	playGame();

}

int main() {

	playGame();

	return 0;

}