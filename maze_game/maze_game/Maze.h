/*

	Maze.h
	maze_game

	Created by Jaxon Kneipp.
	Copyright � 2017 Jaxon Kneipp. All rights reserved.

*/

// Include libraries and required objects
#pragma once
#include <string>
#include "Player.h"

using namespace std;

// Declaration of Maze class
class Maze {

	private:

		bool is_empty_row(int row[70]);

	public:

		Player player;

		int score = 0;

		int side_length = 70;

		bool completed = false;

		int bitmap[70][70];

		int level = 1;

		int amount_of_levels = 4;

		int level_starting_positions[4][2] = { {1, 0}, {1, 0}, {1, 0}, { 1, 0 } };
		int level_ending_position[4][2] = { { 13, 31 },{ 15, 31 },{ 17, 43 }, {9, 28} };

		void move_character(char direction_code);

		void load_current_level();

		void print();

};
